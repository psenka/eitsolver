import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.lines import Line2D
import matplotlib.ticker as mtick
from matplotlib import rc

rc('font', **{'size': 16})
rc('text', usetex=True)


def rsd_el_ticks():
    layer_el_no = [45, 41, 37, 32, 27, 22, 16, 10, 4]
    return [sum(layer_el_no[:i]) for i in range(len(layer_el_no))]


class Comp:
    def __init__(self, sol1, sol2, tag1=r'Solution $1$', tag2=r'Solution $2$'):
        """
        @type sol1: np.ndarray
        @type sol2: np.ndarray
        @type tag1: str
        @type tag2: str
        """
        if np.size(sol1) != np.size(sol2):
            raise Exception("Comparison of incompatible data")
        self.sol1 = sol1
        self.sol2 = sol2
        self.tag1 = tag1
        self.tag2 = tag2

    def size(self):
        return self.sol1.shape[0]

    def diff_file(self, fname):
        """
        @type fname: str
        @return:
        """
        outfile = open(fname, 'w')
        for x in (self.sol1 - self.sol2):
            outfile.write('{0:f}\n'.format(x))

    def diff_plot(self, fname):
        """
        @type fname: str
        """
        fig = plt.figure()
        ax1 = fig.add_subplot(1, 1, 1)
        ax1.set_xlabel(r'Index elektrody $i$')
        ax1.set_ylabel(r'Nap\v{e}t\'{i} [mV]')
        h1 = ax1.plot(self.sol1, label=self.tag1)
        h2 = ax1.plot(self.sol2, label=self.tag2)
        ax1.legend(handles=[h1[0], h2[0]], loc=3)
        ax1.set_xlim([-5, 239])
        ax2 = plt.twinx(ax1)
        ax2.set_ylabel(r'Rozd\'{i}l nap\v{e}t\'{i} [mV]', color='red')
        ax2.plot(self.sol1 - self.sol2, 'r-')
        ax2.tick_params(axis='y', colors='red')
        ax2.set_xlim([-5, 239])
        plt.tight_layout()
        fig.savefig(fname)

    def diffmat(self):
        """
        @rtype np.ndarray:
        """
        N = self.size()
        diffmat = np.zeros(N**2).reshape(N, N)
        for i in range(N - 1):
            for j in range(i + 1, N):
                diffmat[i, j] = self.sol1[i] - self.sol2[j]
                diffmat[j, i] = self.sol1[i] - self.sol2[j]
        return diffmat

    def diffmat_histogram(self, fname, save=True, bins=1000):
        """
        @param bins: Number of bins of the histogram
        @type fname: str
        @type save: bool
        @type bins: int
        """
        n, bins, patches = plt.hist(self.diffmat().flatten(), bins)
        if save:
            plt.savefig(fname)
        return n, bins, patches

    def plot_diffmat(self, fname, vmin=-40.0, vmax=100.0,
                     ticks=rsd_el_ticks()):
        """
        @param fname: Filename to store the diffmat plot
        @param vmin: Minimum of colorbar
        @param vmax: Maximum of colorbar
        @param ticks:
        @type fname: str
        @type vmin: float
        @type vmax: float
        @type ticks: list[int]
        """
        fig, ax = plt.subplots()
        divider = make_axes_locatable(ax)
        cax = divider.append_axes('right', size='5%', pad=0.05)

        im = ax.matshow(self.diffmat(), vmin=vmin, vmax=vmax)
        l = Line2D([0, 234], [0, 234], color='k')
        ax.add_line(l)
        ax.set_xlabel(r'$j$')
        ax.set_ylabel(r'$i$')
        ax.set_xticks(ticks)
        ax.set_xticklabels(ticks, rotation='vertical')
        ax.set_yticks(ticks)
        ax.set_yticklabels(ticks)
        ax.tick_params(axis='both', which='major', labelsize=13)

        cbar = fig.colorbar(im, cax=cax, orientation='vertical')
        cbar.set_label(r'Nap\v{e}t\'{i} [mV]')
        plt.savefig(fname)

    @staticmethod
    def from_files(fname1, fname2, tag1, tag2):
        """
        @type fname1: str
        @type fname2: str
        @type tag1: str
        @type tag2: str
        @rtype Comp
        """
        return Comp(np.loadtxt(fname1), np.loadtxt(fname2), tag1, tag2)

