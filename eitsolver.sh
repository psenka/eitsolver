#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "Usage: ./eitsolver.sh version"
    echo "Where version is the desired fenics version"
fi

FILESTUB="eitsolver_$(date +"%F-%H-%M-%S")"
echo "Started on $(date)" >> "$FILESTUB.stdout"
fenicsproject run "$1" python forward.py 1>"$FILESTUB.stdout" 2>"$FILESTUB.stderr"
echo "Finished on $(date)" >> "$FILESTUB.stdout"
