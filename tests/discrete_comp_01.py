from discrete_comp import *

test_comp = Comp.from_files('prs3D.dat', 'eitsolver.dat', 'prs3D', 'eitsolver')

test_comp.plot_diffmat('diffmat.pdf')
test_comp.diff_plot('diff.pdf')
