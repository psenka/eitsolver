# This script computes the positions of electrodes in the RSD prototype
# * outputs the data to pos_electrodes.dat
# * creates a .geo file of electrode positions to be used in gmsh
# * outputs a 3D, 2D and a 2D numbered figure
# elPos.pdf, elPos_2D.pdf, elPos_2D_num.pdf

import sys
import numpy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

if len(sys.argv) != 3:
    print "Usage : python get_pos_electrodes b"
    print "Where b = Y/N whether you want graphical display or not"
    sys.exit()

display = (sys.argv[2] == 'Y')
outfile = sys.argv[1]

# radius sfery, pocet vrstev, uhly odpovidajici prvni a posledni vrstve
sphere_radius = 0.065
no_layers = 9
start_angle, end_angle = 30*numpy.pi/180, 86*numpy.pi/180

# theta odpovidajici jednotlivym vrstvam
angles = numpy.linspace(start_angle, end_angle, no_layers)

# polomery kruznic na kterych lezi elektrody v dane vrstve
radii = numpy.cos(angles)*sphere_radius

# z souradnice elektrod v dane vrstve
h = numpy.sin(angles)*sphere_radius
circumferences = 2*numpy.pi*radii

# pole ktere budou obsahovat souradnice
X, Y, Z = numpy.zeros(0), numpy.zeros(0), numpy.zeros(0)

# delka oblouku (fi = konst) mezi dvema vrstvami
layer_arc = sphere_radius*(end_angle-start_angle)/(no_layers-1)

# pocet elektrod, ktere se vmestnaji na jednu vrstvu tak, aby uhlova vzdalenost
# mezi sousednimi byla layer_arc
no_layer_electrodes = numpy.round(circumferences/layer_arc)
no_layer_electrodes = no_layer_electrodes.astype(int)

print('Total number of electrodes: ')
print(numpy.sum(no_layer_electrodes))
print(angles.size)
print('Number of electrodes in individual layers : ')
print(no_layer_electrodes)

# vypocet souradnic
for i in range(angles.size):
        layer_phi = numpy.array(range(no_layer_electrodes[i]))* \
        (2*numpy.pi/no_layer_electrodes[i])
        X = numpy.append(X, radii[i]*numpy.cos(layer_phi))
        Y = numpy.append(Y, radii[i]*numpy.sin(layer_phi))
        Z = numpy.append(Z, numpy.ones(no_layer_electrodes[i])*h[i])

output_file = open(
    outfile, 'w')
outfile = outfile.rsplit('.',1)[0] + '.gmsh'
output_file_geo = open(
    outfile, 'w')
output_file_geo.write('lc = 1e-2;\n')

for i in range(sum(no_layer_electrodes)):
    output_file.write('{0:8.8f} {1:8.8f} {2:8.8f}\n'.format(X[i], Y[i], Z[i]))
    output_file_geo.write(
        'Point({0}) = {{ {1:8.8f}, {2:8.8f}, {3:8.8f}, lc }};\n'.format(
            i+1, X[i], Y[i], Z[i])
    )

if display:
    # graphical output
    fig = plt.figure(figsize=plt.figaspect(7.0/8.0))
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(X, Y, Z)
    fig.savefig('./electrode_positions/elPos.pdf')
    fig.show()
    wait = input("Displaying figure. Press ENTER to continue.")

    # 2D Plot
    fig2D = plt.figure()
    ax2D = fig2D.add_axes([0.1, 0.1, 0.8, 0.8])
    xy_coords = ax2D.plot(X, Y, 'bo')
    plt.xlim(-0.07, 0.07)
    plt.ylim(-0.07, 0.07)
    plt.gca().set_aspect('equal', adjustable='box')
    fig2D.savefig('./electrode_positions/elPos_2D.pdf')
    fig2D.show()
    wait = input("Displaying figure. Press ENTER to continue.")

    # Numbered 2D Plot
    fignum = plt.figure()
    axnum = fignum.add_subplot(1, 1, 1)
    # num_coords = axnum.plot(X,Y,'bo')
    plt.xlim(-0.06, 0.06)
    plt.ylim(-0.06, 0.06)
    plt.gca().set_aspect('equal', adjustable='box')

    for i in range(sum(no_layer_electrodes)):
        axnum.text(X[i], Y[i], str(i+1), fontsize=8, bbox=dict(facecolor='red',
            alpha=0.5, boxstyle='circle', pad=0.25))

    plt.tight_layout()
    fignum.savefig('./electrode_positions/elPos_2D_num.pdf')
    fignum.show()
    wait = input("Displaying figure. Press ENTER to continue.")
