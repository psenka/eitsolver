import dolfin as df
import ufl
import numpy as np
import datetime as dt
import ConfigParser as cfg
from abc import ABCMeta, abstractmethod

df.parameters["form_compiler"]["representation"] = "uflacs"


class Electrode(df.SubDomain):
    __metaclass__ = ABCMeta

    def __init__(self, pos, impedance=1.0):
        """
        @type pos: np.ndarray
        @type impedance: float
        """
        df.SubDomain.__init__(self)
        self._pos = pos
        self.impedance = impedance

    def pos(self):
        return self._pos

    @abstractmethod
    def inside(self, x, on_boundary):
        """
        @type x: np.ndarray
        @type on_boundary : bool
        @rtype bool
        @return:
        """
        pass


class CircularElectrode(Electrode):
    def __init__(self, pos, impedance, radius):
        Electrode.__init__(self, pos, impedance)
        self.radius = radius

    def inside(self, x, on_boundary):
        """
        @type x: np.ndarray
        @type on_boundary: bool
        @rtype bool
        @return:
        """
        return on_boundary and (np.linalg.norm(x - self._pos) <= self.radius)

    @staticmethod
    def load_default_electrodes(infile):
        """
        @type infile: str
        @rtype list[Electrode]
        @return:
        """
        positions = np.loadtxt(infile)
        if not positions.shape[0] > 0:
            raise Exception("Electrode file is empty!")
        return [CircularElectrode(pos, 1.0, 0.0025) for pos in positions]


class StimulationPattern:
    def __init__(self, arr):
        """
        @type arr: np.ndarray
        @param arr:
        """
        if not arr.shape[0] > 0:
            raise Exception('Invalid size of array argument!')
        if not np.sum(arr) == 0:
            raise Exception('Array argument cannot be interperted as a '
                            'stimulation pattern!')
        self.currents = arr
        self._el_no = arr.shape[0]

    def el_no(self):
        return self._el_no

    @staticmethod
    def make_simple_stim(no_el, i, j, amp):
        """
        @type no_el : int
        @type i: int
        @type j: int
        @type amp: float
        @rtype StimulationPattern
        """
        arr = np.zeros(no_el, dtype=float)
        arr[i] = amp
        arr[j] = -amp
        return StimulationPattern(arr)

    @staticmethod
    def load_simple_stims(filename):
        """
        @type filename: str
        @param filename: .stim file to load simple stims from
        @rtype: list[StimulationPattern]
        """
        infile = file(filename)
        firstline = infile.readline()
        no_el = int(firstline.split()[0])
        stims = []
        if (no_el <= 0) or (len(firstline.split()) != 1):
            raise Exception('First line of stimulation file must state a valid'
                            'number of electrodes!')
        for line in infile:
            stim_data = line.split()
            if len(stim_data) != 3:
                print line
                raise Exception('Invalid format of .stim file entry')
            else:
                (el_id_1, el_id_2, amp) = stim_data
                if (int(el_id_1) < 0) or (int(el_id_1) >= no_el):
                    print line
                    raise Exception('Invalid index of first electrode of '
                                    'simple stim pattern')
                elif (int(el_id_2) < 0) or (int(el_id_2) >= no_el):
                    print line
                    raise Exception('Invalid index of second electrode of '
                                    'simple stim pattern')
                else:
                    stims.append(StimulationPattern.make_simple_stim(
                        no_el, int(el_id_1), int(el_id_2), float(amp)))
        return stims


class ForwardModel(object):
    def __init__(self, name, mesh, electrodes, stimulation,
                 elem_family='Lagrange', elem_degree=1):
        """
        @type name: str
        @type mesh: df.Mesh
        @type electrodes: list[Electrode]
        @type stimulation: list[StimulationPattern]
        @type elem_family: str
        @type elem_degree: int
        """
        self._name = name
        self.mesh = mesh
        self.electrodes = electrodes
        self._elem_family = elem_family
        self._elem_degree = elem_degree
        self._el_no = len(electrodes)
        self.stimulation = stimulation

    @staticmethod
    def from_cfg(cfg_file):
        """
        @type cfg_file: str
        @param cfg_file: Source .cfg file
        @rtype: ForwardSolver
        """
        parser = cfg.ConfigParser()
        parser.read(cfg_file)
        cfg_section = 'ForwardModel'
        if not parser.has_section(cfg_section):
            raise Exception("Config file does not contain a {0}"
                            " section!".format(cfg_section))

        need = {'electrode_file': None, 'stimulation_file': None,
                'name': None, 'mesh_file': None}

        def load_options():
            for option in need.keys():
                if parser.has_option(cfg_section, option):
                    need[option] = parser.get(cfg_section, option)
                else:
                    raise Exception("Option {0} not specified"
                                    " in config file".format(option))

        load_options()
        electrodes = CircularElectrode.load_default_electrodes(
            need['electrode_file'])
        stimulation = StimulationPattern.load_simple_stims(
            need['stimulation_file'])
        model = ForwardModel(need['name'],
                             df.Mesh(need['mesh_file']),
                             electrodes,
                             stimulation
                             )
        return model

    def elem_family(self):
        return self._elem_family

    def elem_degree(self):
        return self._elem_degree

    def display_mesh(self):
        df.plot(self.mesh, interactive=True)

    def el_no(self):
        return self._el_no

    def name(self):
        return self._name

    def el_pos(self, k):
        return self.electrodes[k].pos()


class ForwardSolver:
    def __init__(self, model):
        """
        @type model: ForwardModel
        """
        self.model = model

        P1 = df.FiniteElement(self.model.elem_family(),
                              self.model.mesh.ufl_cell(),
                              self.model.elem_degree()
                              )
        R_el = df.VectorElement("R", self.model.mesh.ufl_cell(), 0,
                                dim=self.model.el_no()
                                )
        R = df.FiniteElement("R", self.model.mesh.ufl_cell(), 0)
        mixed_element = df.MixedElement([P1, R_el, R])
        self.solution_space = df.FunctionSpace(self.model.mesh, mixed_element)

        self.sigma = ForwardSolver._temp_sigma()
        self.A = self._assemble_lhs2()
        self.solutions = {}

    def _init_ds(self):
        el_marker = df.FacetFunction("size_t", self.model.mesh)
        el_marker.set_all(9999)
        for i in range(len(self.model.electrodes)):
            self.model.electrodes[i].mark(el_marker, i)
        return df.Measure('ds', domain=self.model.mesh,
                          subdomain_data=el_marker)

    @staticmethod
    def _temp_sigma():
        return df.Constant(1.0)

    def _assemble_lhs(self):
        # First initialize the surface measure providing integrals over
        # electrode surfaces
        ds = self._init_ds()
        u, U, c = df.TrialFunctions(self.solution_space)
        v, V, d = df.TestFunctions(self.solution_space)
        summands = [
            df.Constant(1.0/self.model.electrodes[i].impedance) *
            (U[i] - u) * (V[i] - v) * ds(i)
            for i in range(self.model.el_no())
        ]
        el_term = ufl.Form([i for s in summands for i in s.integrals()])

        A = self.sigma * u * v * df.dx + el_term +\
            d*u*df.dx + c*v*df.dx
        return A

    def _assemble_lhs2(self):
        # First initialize the surface measure providing integrals over
        # electrode surfaces
        ds = self._init_ds()
        u, U, c = df.TrialFunctions(self.solution_space)
        v, V, d = df.TestFunctions(self.solution_space)
        el_summands = [
            df.Constant(1.0/self.model.electrodes[i].impedance) *
            (U[i] - u) * (V[i] - v) * ds(i)
            for i in range(self.model.el_no())
        ]
        A = self.sigma * df.inner(df.grad(u), df.grad(v)) * df.dx +\
            sum(el_summands) + d*u*df.dx + c*v*df.dx
        return A

    def _assemble_rhs(self, m):
        """
        @type m : int
        @param m: Index of stimulation pattern to assemble the rhs for
        @return:
        """
        if (m < 0) or (m > len(self.model.stimulation)):
            raise Exception('Cannot assemble right hand side - invalid index'
                            'of stimulation pattern!')
        v, V, d = df.TestFunctions(self.solution_space)

        volume = df.assemble(df.Constant(1.0)*df.dx(self.model.mesh))
        weight = df.Constant(1./volume)

        summands = [
            df.Constant(self.model.stimulation[m].currents[i]) * V[i] *
            weight * df.dx
            for i in range(self.model.el_no())
        ]
        L = ufl.Form([i for s in summands for i in s.integrals()])

        return L

    def simple_solve(self, m):
        """
        @param m: Index of stimulation pattern to assemble the rhs for
        @type m: int
        """
        solution = df.Function(self.solution_space)
        L = self._assemble_rhs(m)
        df.solve(self.A == L, solution)
        self.solutions[m] = solution

    def timed_solve(self, m):
        """
        @param m: Index of stimulation pattern to assemble the rhs for
        @type m: int
        """
        solution = df.Function(self.solution_space)
        L = self._assemble_rhs(m)
        startTime = dt.datetime.now()
        a = df.assemble(self.A)
        print "ForwardSolver.timed_solve : LHS assembly time : "
        print dt.datetime.now() - startTime
        startTime = dt.datetime.now()
        b = df.assemble(L)
        print "ForwardSolver.timed_solve : RHS assembly time : "
        print dt.datetime.now() - startTime
        startTime = dt.datetime.now()
        df.solve(a, solution.vector(), b)
        print "ForwardSolver.timed_solve : Linear system solve time : "
        print dt.datetime.now() - startTime
        self.solutions[m] = solution

    def display_electrodes(self):
        df.plot(self.ds.subdomain_data(), interactive=True)

    def display_solution(self, m):
        """
        @param m: Index of stimulation pattern the corresponding solution of
                    which is supposed to be displayed
        @type m: int
        """
        if m not in self.solutions:
            raise Exception('Invalid index of solution to be output')
        (u, U, c) = self.solutions[m].split()
        df.plot(u, interactive=True)

    def el_potential_tostring(self, m):
        """
        @param m: Index of stimulation pattern the corresponding electrode
                    potentials of which are supposed to be output as string
        @type m: int
        @rtype str
        """
        if m not in self.solutions:
            raise Exception('Invalid index of solution to be output')

        (u, U, c) = self.solutions[m].split()
        string = ''
        proxy = df.Point(self.model.mesh.coordinates()[0])
        for i in range(self.model.el_no()):
            string += '{0:f}\t'.format(U.sub(i)(proxy))
        string += '\n'

        return string

    def output_solution(self, m, fmt):
        """
        @param m: Index of stimulation pattern the corresponding solution of
                    which is supposed to be output in the given 'format'
        @type m: int
        @type fmt : str
        """
        if m not in self.solutions:
            raise Exception('Invalid index of solution to be output')

        (u, U, c) = self.solutions[m].split()

        filename = 'results/' + self.model.name() + \
                   '_{0:0>5d}.'.format(m) + fmt
        outfile = df.File(filename)
        outfile << u
        outfile2 = file('results/' + self.model.name() + '_U.dat', 'a+')
        outfile2.write(self.el_potential_tostring(m))

model = ForwardModel.from_cfg('models/rsd_mesh/rsd_mesh.cfg')
solver = ForwardSolver(model)
solver.timed_solve(0)
