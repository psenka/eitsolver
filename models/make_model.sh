if [ "$#" -ne 4 ]; then
    echo "Invalid number of arguments!"
    exit 1
fi

model_basename=$(basename $1)
model_name="${model_basename%.*}"

# $cfg_file="$model_name/$model_name.cfg"
cfg_file="$1"

if [ -e "$1" ]; then
    rm "$1"
fi

touch "$cfg_file"
echo "[ForwardModel]" >> "$cfg_file"
echo "electrode_file=$2" >> "$cfg_file"
echo "stimulation_file=$4" >> "$cfg_file" 
echo "name=$model_name" >> "$cfg_file"
echo "mesh_file=$3" >> "$cfg_file"

