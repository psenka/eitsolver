MESHES_GEO = $(shell find -type f -name '*.geo')
MESHES_CFG = $(MESHES_GEO:.geo=.cfg)
MESHES_EL = $(MESHES_GEO:.geo=.el)
MESHES_XML = $(MESHES_GEO:.geo=.xml)

EL_POS_SCRIPT = get_pos_electrodes.py
EL_POS_SCRIPT_FLAGS = N

RESULTS_DIR = results
MODELS_DIR = models
DIRS = ${RESULTS_DIR}

.PHONY: ${DIRS}

all : ${RESULTS_DIR} ${MESHES_EL} ${MESHES_XML} ${MESHES_CFG}

%.cfg : %.el %.xml %.stim
		sh ${MODELS_DIR}/make_model.sh $@ $^

%.el : ${EL_POS_SCRIPT}
		python ${EL_POS_SCRIPT} $@ ${EL_POS_SCRIPT_FLAGS}

%.xml : %.msh
		dolfin-convert $< $@

%.msh : %.geo
		gmsh -3 -o $@ $<

${RESULTS_DIR} :
	mkdir -p ${RESULTS_DIR}

clean :
	rm *.stderr *.stdout ${RESULTS_DIR}/*.pvd ${RESULTS_DIR}/*.vtu
	instant-clean
